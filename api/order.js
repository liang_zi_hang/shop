import Request from '.././plugins/request/index';

export function getlist(url, data) {
    return Request().request({
        url,
        method: 'get',
		data
	})
}

// 剔除字符串里面的字母
export function dislodgeLetter(str) {
    let result;
   let reg = /[a-zA-Z]+/;  //[a-zA-Z]表示匹配字母，g表示全局匹配
   while (result = str.match(reg)) { //判断str.match(reg)是否没有字母了
       str = str.replace(result[0], ''); //替换掉字母  result[0] 是 str.match(reg)匹配到的字母
   }
   return str;
   }
