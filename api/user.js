import Request from '.././plugins/request/index';

/**
 * 统一弹窗
 * 
 * 
 */
export function msg(title, duration=1500, mask=false, icon='none'){
	//统一提示方便全局修改
	if(Boolean(title) === false){
		return;
	}
	uni.showToast({
		title,
		duration,
		mask,
		icon
	});
}



/**
 * 获取用户信息
 * @param {string} path
 * @param {int} id
 */
export function getuser(url) {
    return Request().request({
        url,
        method: 'GET'
    })
}

