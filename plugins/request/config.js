import Interceptor from './core/interceptor';
import Request from './index';
import TokenApi from '../.././api/token';
import {msg} from '../.././api/publicapi'

export const globalInterceptor = {
	request: new Interceptor(),
	response: new Interceptor()
};

/**
 * 全局配置
 * 只能配置 静态数据
 * `content-type` 默认为 application/json 
 * header 中`content-type`设置特殊参数 或 配置其他会导致触发 跨域 问题，出现跨域会直接进入响应拦截器的catch函数中
 */
export const config = {
	baseURL: 'https://edadmin.ed163.net/api/',
    
	// dataType: 'json',
	// responseType: 'text',
	header: {
		// uid: 'xxxx',
		contentType: 'application/x-www-form-urlencoded'
		// 'Content-Type': 'application/json'
	}
};

/**
 * 全局 请求拦截器
 * 例如: 配置token
 *
 * `return config` 继续发送请求
 * `return false` 会停止发送请求，不会进入错误数据拦截，也不会进入请求对象中的catch函数中
 * `return Promise.reject('xxxxx')` 停止发送请求, 会错误数据拦截，也会进入catch函数中
 *
 * @param {Object} config 发送请求的配置数据
 */
globalInterceptor.request.use(
	(config) => {
		if (config.header.Authorization == undefined) {
			if (getToken() !== false && getToken() !== null) {
				let authToken = getToken()
				config.header.Authorization = authToken.token_type + ' ' + authToken.access_token
			}


		}

		// config.header.Authorization =
		// 	'bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkua29vcHouY246ODg4OFwvYXBpXC9mcm9udFwvYXV0aF9jb2RlIiwiaWF0IjoxNTczNTIzMTcyLCJleHAiOjE1NzM1NDQ3NzIsIm5iZiI6MTU3MzUyMzE3MiwianRpIjoiWEhrYXZYdE5vZ1QydlFjayIsInN1YiI6MTc3MywicHJ2IjoiODYzZmRkYWUzMWY0NzgzZDNlMzhlZmY2OWQwNWQzNjU4YzcyMjMxYyJ9.SNG9Rp9z7JgBec6iBpQMHUlEvOwGtiDGSfydcSZs2N4'

		return config;
		// return false;
		// return Promise.reject('is error')
	},
	(err) => {
		console.error('is global fail request interceptor: ', err);
		return false;
	}
);

// 支持添加多个请求、响应拦截器
// globalInterceptor.request.use(config => {
//     console.log('is global request interceptor 2');
//     return config;
// }, err => {
//     console.error('global request: ', err);
//     return false;
// });

/**
 * 全局 响应拦截器
 * 例如: 根据状态码选择性拦截、过滤转换数据
 *
 * `return res` 继续返回数据
 * `return false` 停止返回数据，不会进入错误数据拦截，也不会进入catch函数中
 * `return Promise.reject('xxxxx')` 返回错误信息, 会错误数据拦截，也会进入catch函数中
 *
 * @param {Object} res 请求返回的数据
 * @param {Object} config 发送请求的配置数据
 * @return {Object|Boolean|Promise<reject>}
 */
globalInterceptor.response.use(
	(res, config) => {

		if (res.data !== undefined && res.data.data !== undefined && res.data.data.access_token !== undefined) {
			const token = {
				access_token: res.data.data.access_token,
				refresh_expire: res.data.data.refresh_expire,
				token_expire: res.data.data.token_expire,
				token_type: res.data.data.token_type
			}
			saveToken(token)

		}
		const code = parseInt(res.data.code);
		if (code == 406 && getToken() !== null) {
			// token 验证失败, 并且这个实例是第一次重复请求
			config.url = config.instanceURL;
			return getApiToken().then(() => Request().request(config))
        }
        const status_code = parseInt(res.data.status_code)
        if (code == 406 && getToken() === null || status_code ==500) {
        	msg('您尚未登录')
        }
		return res.data;

	},
	(err, config) => {
		console.error('is global response fail interceptor');
		console.error('err: ', err);
		console.error('config: ', config);
		const {
			errMsg,
			data
		} = err;
		return Promise.reject({
			errMsg,
			data,
			config
		});


	}
);

// 重新请求更新获取 token
async function getApiToken() {
	const res = await TokenApi.getMockToken();
	const token = false
	if (res.data.data != undefined) {
		token = res.data.data;
	} else {
		uni.removeStorageSync('token');
	}

	console.log(token)
	return token;
}

// 获取 localStorage 中的 token
function getToken() {
	let token = uni.getStorageSync('token')

	if (token) {
		token = JSON.parse(token)
		// 判断时间是否超时
		// console.log( (token.get_time + token.token_expire) )
		// console.log( )
		if ((token.get_time + token.token_expire) < (Date.parse(new Date()) / 1000)) {
			return false
		}
		return token
	} else {
		return null
	}
}

// 保存 token 到 localStorage
function saveToken(obj) {
	console.log(obj)
	let token = {
		access_token: obj.access_token,
		refresh_expire: obj.refresh_expire,
		token_expire: obj.token_expire,
		token_type: obj.token_type
	}
	token = Object.assign({}, token, {
		get_time: (Date.parse(new Date()) / 1000)
	})
	uni.setStorageSync('token', JSON.stringify(token));
}
