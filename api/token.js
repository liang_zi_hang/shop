import Request from '.././plugins/request/index';
 
export default {
    // 获取 模拟token
	
    getMockToken () {
		let  authToken = this.gettoken()
        return Request().post('front/refreshtoken', {header: {
				Authorization : authToken.token_type + ' ' + authToken.access_token
			}});
    },

    // 验证 模拟token，id为 2460392754 则 success，code 200， 否则 fail code 为 401
    checkMockToken (token) {
        return Request().request({
            url: 'front/refreshtoken',
            method: 'POST',	
            count: 0 // 用来记录 每个实例请求的 请求次数（可以用来判断 重新发送请求的次数）
        });
    }, 
	gettoken(){
		 let token = uni.getStorageSync('token')
		
		   token = JSON.parse(token)
		 return token
	}
}