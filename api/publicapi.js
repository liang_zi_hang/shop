import Request from '.././plugins/request/index';

/**
 * 统一弹窗
 * 
 * 
 */
export function msg(title, duration=1500, mask=false, icon='none'){
	//统一提示方便全局修改
	if(Boolean(title) === false){
		return;
	}
	uni.showToast({
		title,
		duration,
		mask,
		icon
	});
}
// 获取上一页实例
//  export const prePage = ()=>{
// 	let pages = getCurrentPages();
// 	let prePage = pages[pages.length - 2];
// 	// #ifdef H5
// 	return prePage;
// 	// #endif
// 	return prePage.$vm;
// }
export function onShareAppMessage(res) {
    if (res.from === 'button') {// 来自页面内分享按钮
      console.log(res.target)
    }
    return {
      title: '自定义分享标题',
      path: '/pages/test/test?id=123'
    }
  }
/**
 * 统一跳转
 * 
 * 
 */

export function navTo(url){
		uni.navigateTo({  
			url
		})  
	}

/**
 * 获取单条数据
 * @param {string} path
 * @param {int} id
 */
export function getData(path, id) {
    const url = id >= 0 ? path + '/' + id : path
    return Request().request({
        url,
        method: 'GET'
    })
}

/**
 * 获取单条数据
 * @param {string} path
 * @param {int} id
 */
export function postData(path, id) {
    const url = id >= 0 ? path + '/' + id : path
    return Request().request({
        url,
        method: 'POST'
    })
}

/**
 * 获取数据
 * @param {string} url
 * @param {opj} data
 */
export function getOne(url, data) {
    return Request().request({
        url,
        method: 'GET',
		data
	})
}

/**
 * 获取数据
 * @param {string} url
 * @param {opj} data
 */
export function postOne(url, data) {
  
    return Request().request({
        url,
        method: 'POST',
		data
	})
}

/**
 * 获取数据
 * @param {string} url
 * @param {opj} data
 */
export function putOne(url, data) {
    
    return Request().request({
        url,
        method: 'PUT',
		data
	})
}


/**
 * 获取列表
 * @param {string} path
 */
export function getList(path, data) {
    if (data) {
        path = path + '?' + obj2arr(data).join('&')
    }
    return Request().request({
        url: path,
        method: 'GET'
    })
}

/**
 * 删除单条数据
 */
export function destroyData(path, id) {
    const url = path + '/' + id
    return Request().request({
        url,
        method: 'DELETE'
    })
}

/**
 * 提交保存数据
 */
export function saveData(path, id, data, method) {
    const url = id ? path + '/' + id : path

    if (!method) {
        method = id ? 'PUT' : 'POST'
    // return
    }
    return Request().request({
        url,
        data,
        method
    })
}

/**
 * 提交添加数据
 */
export function insertData(path, data) {
    const url = path
    return Request().request({
        url,
        data,
        method: 'POST'
    })
}
/**
 * 提交添加数据
 */
export function storeData(path, data) {
    return Request().request({
        url: path,
        data,
        method: 'store'
    })
}

/**
 * 提交更新排序
 */
export function saveOrder(path, data) {
    return Request().request({
        url: path,
        data,
        method: 'POST'
    })
}

// 剔除字符串里面的字母
export function dislodgeLetter(str) {
    let result;
   let reg = /[a-zA-Z]+/;  //[a-zA-Z]表示匹配字母，g表示全局匹配
   while (result = str.match(reg)) { //判断str.match(reg)是否没有字母了
       str = str.replace(result[0], ''); //替换掉字母  result[0] 是 str.match(reg)匹配到的字母
   }
   return str;
   }
